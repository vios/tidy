# Rubbish

Clean your directories with ease from your terminal. Rubbish is a file deletion only tool with quick response times and a preview window in case you want to review before destruction.

## Dependencies
- [bat](https://github.com/sharkdp/bat)

## Usage

```sh
rubbish [FLAGS] [path]
```

### Flags:

```sh
-h, --help        Prints help information
-x, --nerdless    Disable Nerdfont icons
-q, --quiet       Silence all log output
-V, --version     Prints version information
-v, --verbose     Verbose mode (-v, -vv, -vvv, etc. Useful if you want to check what the tool is doing.)
```

### Arguments

```sh
<path>  The directory that you want to clean. If not set the current working directory will be used
```