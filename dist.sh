#!/bin/bash

cargo update --verbose
cargo build --verbose --frozen --all-features --release --target x86_64-unknown-linux-musl

