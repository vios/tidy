use log::{error, warn};
use pbr::ProgressBar;
use skim::{Skim, SkimOptionsBuilder};
use std::default::Default;
use std::env;
use std::fs;
use std::io::{stdin, stdout, Cursor, Write};
use std::path::PathBuf;
use stderrlog;
use structopt::*;
use termion::clear;
use termion::color;
use termion::event::Key;
use termion::input::TermRead;
use termion::screen::AlternateScreen;

const SPLIT_TAG: &str = " - ";

/// Remove the rubbish from your system!
#[derive(StructOpt, Debug, Clone)]
#[structopt()]
struct Settings {
    /// Silence all log output
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,
    /// Verbose mode (-v, -vv, -vvv, etc. Useful if you want to check what the tool is doing.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,
    /// Disable Nerdfont icons
    #[structopt(short = "x", long = "nerdless")]
    icons_disabled: bool,
    /// The directory that you want to clean. If not set the current working directory will be used
    #[structopt(parse(from_os_str))]
    path: Option<PathBuf>,
}

fn exit_app(code: i32) {
    print!("{}", color::Fg(color::Reset));
    std::process::exit(code)
}

fn show_dir(path: PathBuf, settings: Settings) -> std::io::Result<()> {
    let mut dirs: Vec<String> = Vec::new();
    let mut files: Vec<String> = Vec::new();
    let full_path = fs::canonicalize(&path)?;
    if full_path.is_dir() {
        for entry in fs::read_dir(full_path)? {
            let entry = entry?;
            let etype = entry.file_type()?;

            let path = entry.path();
            let filename = path.to_str().unwrap_or("FAIL_READ");
            let icon = if settings.icons_disabled {
                "+"
            } else if etype.is_dir() {
                "\u{f752}"
            } else if etype.is_file() {
                "\u{f718}"
            } else if etype.is_symlink() {
                "\u{f838}"
            } else {
                "\u{f7d6}"
            };
            let entry_txt = format!("{} - {}\n", icon, filename);
            if etype.is_dir() {
                dirs.push(entry_txt);
            } else {
                files.push(entry_txt);
            }
        }
    } else {
        error!("{:?} is not a directory", &path);
        exit_app(1);
    }

    dirs.append(&mut files);
    let input: String = dirs.iter().flat_map(|s| s.chars()).collect();

    let options = SkimOptionsBuilder::default()
        .multi(true)
        .prompt(Some("<ESC> = Exit, <Enter> = Start, <Tab> Select | File > "))
        .header(Some("### Rubbish - File deletion tool ###"))
        .preview(Some("bat --color=always {2}"))
        .no_height(true)
        .delimiter(Some(SPLIT_TAG))
        .cmd(Some("rg --color=always \"{}\""))
        .nth(Some("2"))
        .with_nth(Some("1,2"))
        .tabstop(Some("4"))
        .layout("reverse")
        .color(Some("dark,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe,info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef"))
        .build()
        .unwrap();

    let selected_items = Skim::run_with(&options, Some(Box::new(Cursor::new(input))))
        .map(|out| out.selected_items)
        .unwrap_or_else(|| Vec::new());

    let item_count = selected_items.len();
    if item_count <= 0 {
        exit_app(127)
    }

    {
        let mut confirm_screen = AlternateScreen::from(stdout());
        writeln!(
            confirm_screen,
            "\n{}== Confirm delete request =={}\n",
            color::Fg(color::Red),
            color::Fg(color::Reset)
        )?;
        for item in selected_items.iter() {
            writeln!(
                confirm_screen,
                "\t{}{}",
                color::Fg(color::Cyan),
                item.get_output_text(),
            )?;
        }
        write!(confirm_screen, "{}", color::Fg(color::Reset))?;
        write!(
            confirm_screen,
            "{}{}",
            color::Fg(color::Blue),
            "Continue? [y/n]: "
        )?;

        confirm_screen.flush().unwrap();
        loop {
            let mut answer = 0;
            for k in stdin().lock().keys() {
                if k.is_err() {
                    warn!("keyscan error");
                    continue;
                }
                let keys = k.unwrap();
                let choice = match keys {
                    Key::Char('Y') | Key::Char('y') => 1,
                    Key::Char('N') | Key::Char('n') => 2,
                    _ => {
                        writeln!(
                            confirm_screen,
                            "{}{}",
                            "Invalid answer. Try again",
                            color::Fg(color::Yellow)
                        )
                        .unwrap();
                        -1
                    }
                };
                if choice > 0 {
                    answer = choice;
                    break;
                }
            }
            confirm_screen.flush().unwrap();
            match answer {
                1 => break,
                2 => exit_app(127),
                _ => (),
            }
        }
    }
    write!(stdout().lock(), "{}", clear::All)?;
    let mut pb = ProgressBar::new(item_count as u64);
    pb.format("╢▌▌░╟");
    let mut success: Vec<String> = Vec::new();
    for item in selected_items.iter() {
        let full = item.get_output_text().clone();
        let parts = item.get_text().split(SPLIT_TAG);
        let to_delete = parts.skip(1).next().unwrap_or("");
        if to_delete.len() > 0 {
            let meta = fs::metadata(to_delete)?;
            if meta.is_dir() {
                if let Err(error) = fs::remove_dir_all(to_delete) {
                    error!("Delete directory failed: {}", error);
                } else {
                    success.push(full.to_string());
                }
            } else {
                if let Err(error) = fs::remove_file(to_delete) {
                    error!("Delete file failed: {}", error);
                } else {
                    success.push(full.to_string());
                }
            }
        } else {
            warn!("Invalid or missing file. Skipping...");
        }
        pb.inc();
    }
    pb.finish_print("Done!");
    println!("Deleted items:");
    for item in success.iter() {
        println!("\t{}{}", color::Fg(color::Green), item);
    }
    Ok(())
}

pub fn main() -> std::io::Result<()> {
    let mut cfg = Settings::from_args();
    let basic_log_level = match cfg.verbose {
        0 => "warn",
        1 => "info",
        2 => "debug",
        3 | _ => "trace",
    };
    env::set_var("RUST_LOG", basic_log_level);
    env::set_var("RUST_BACKTRACE", "1");

    stderrlog::new()
        .module(module_path!())
        .quiet(cfg.quiet)
        .verbosity(cfg.verbose)
        .timestamp(stderrlog::Timestamp::Off)
        .init()
        .unwrap();
    let target = if cfg.path.is_some() {
        cfg.path.take().unwrap()
    } else {
        std::env::current_dir()?
    };
    show_dir(target, cfg)?;
    Ok(())
}
